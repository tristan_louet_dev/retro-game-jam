local lootLook_mt = {}
lootLook_mt.__index = lootLook_mt

local lootRadius = 5

function newLootLook(world, x, y, color)
	local pl = {}
	
	pl.world = world
	pl.body = love.physics.newBody(pl.world, 0, 0, "static")
    pl.color = color
    pl.shape = love.physics.newCircleShape(lootRadius)
	pl.fixture = love.physics.newFixture(pl.body, pl.shape, 1)
	pl.fixture:setFriction(1)
	pl.body:setPosition(x, y)

	return setmetatable(pl, lootLook_mt)
end

function lootLook_mt:setEntity(entity)
	self.entity = entity
	self.fixture:setUserData(entity);
end

function lootLook_mt:setPosition(x, y)
	self.body:setPosition(x, y)    
end

function lootLook_mt:getPosition()
	return self.body:getPosition(x, y)
end

function lootLook_mt:getLinearVelocity()
	return self.body:getLinearVelocity()
end

function lootLook_mt:setAngle(a)
    
end

function lootLook_mt:getAngle()
	return math.deg(self.body:getAngle())
end

function lootLook_mt:setAngularVelocity(v)
	self.body:setAngularVelocity(v)
end

function lootLook_mt:setSpeed(speed)

end

function lootLook_mt:update(dt)
	
end

function lootLook_mt:explode()
end

function lootLook_mt:setColor(color)
	self.color = color
end

function lootLook_mt:destroyBodies()
	if self.fixture ~= nil then
		self.fixture:destroy()
	end
	self.fixture = nil
end

function lootLook_mt:draw()
	love.graphics.setColor(self.color.red, self.color.green, self.color.blue)
    
	love.graphics.circle("fill", self.body:getX(), self.body:getY(), lootRadius, 8)
	
	-- Affichage de la bounding box
	-- local topLeftX, topLeftY, bottomRightX, bottomRightY = self.fixture:getBoundingBox()
	-- love.graphics.rectangle("line", topLeftX, topLeftY, bottomRightX - topLeftX, bottomRightY - topLeftY)
	
	-- love.graphics.printf("Vie P "..self.entity:getLife(), 0, 25, 300, "left")
	-- love.graphics.printf("angle P "..math.deg(self.body:getAngle()), 0, 60, 300, "left")
	-- love.graphics.printf("angle P "..self.body:getX().." "..self.body:getY(), 0, 40, 300, "left")
end
