local playerLook_mt = {}
playerLook_mt.__index = playerLook_mt

playerHeight = 15
playerWidth = 10

playerStartX = 0
playerStartY = 0

function newPlayerLook(world)
	local pl = {}
	
	pl.world = world
	pl.body = love.physics.newBody(world, playerStartX, playerStartY, "dynamic")
	pl.body:setMassData(0, 0, 10, 0)
	pl.shape = love.physics.newPolygonShape(playerStartX - playerWidth / 2, playerStartY + playerHeight / 2,
									playerStartX + playerWidth / 2, playerStartY + playerHeight / 2,
									playerStartX, playerStartY - playerHeight / 2)
	pl.fixture = love.physics.newFixture(pl.body, pl.shape, 1)
	pl.fixture:setFriction(1)
	pl.speed = 0
	pl.color = createColor(255, 255, 255)

	return setmetatable(pl, playerLook_mt)
end

function playerLook_mt:setEntity(entity)
	self.entity = entity
	self.fixture:setUserData(entity);
end

function playerLook_mt:setPosition(x, y)
	self.body:setPosition(x, y)
end

function playerLook_mt:getPosition()
	return self.body:getPosition(x, y)
end

function playerLook_mt:getLinearVelocity()
	return self.body:getLinearVelocity()
end

function playerLook_mt:setAngle(a)
	a = math.fmod(a, 360)
	self.body:setAngle(math.rad(a))
	self.body:setLinearVelocity(math.sin(self.body:getAngle()) * self.speed, 
								-math.cos(self.body:getAngle()) * self.speed)
end

function playerLook_mt:getAngle()
	return math.deg(self.body:getAngle())
end

function playerLook_mt:setAngularVelocity(v)
	self.body:setAngularVelocity(v)
end

function playerLook_mt:setLinearVelocity(v)
    self.body:setLinearVelocity(v)
end

function playerLook_mt:setSpeed(speed)
	self.speed = speed
	self.body:setLinearVelocity(math.sin(self.body:getAngle()) * speed, 
								-math.cos(self.body:getAngle()) * speed)
end

function playerLook_mt:update(dt)
	self.body:setAngularVelocity(0)
end

function playerLook_mt:explode()
end

function playerLook_mt:setColor(color)
	self.color = color
end

function playerLook_mt:destroyBodies()
	if self.fixture ~= nil then
		self.fixture:destroy()
	end
	self.fixture = nil
end

function playerLook_mt:draw()
	love.graphics.setColor(self.color.red, self.color.green, self.color.blue)
	x1, y1, x2, y2, x3, y3 = self.body:getWorldPoints(
									-playerWidth / 2, playerHeight / 2,
									playerWidth / 2, playerHeight / 2,
									0, -playerHeight / 2)
	love.graphics.triangle("line", x1, y1, x2, y2, x3, y3)
	-- Affichage de la bounding box
	-- local topLeftX, topLeftY, bottomRightX, bottomRightY = self.fixture:getBoundingBox()
	-- love.graphics.rectangle("line", topLeftX, topLeftY, bottomRightX - topLeftX, bottomRightY - topLeftY)
	
	-- love.graphics.printf("Vie P "..self.entity:getLife(), 0, 25, 300, "left")
	-- love.graphics.printf("angle P "..math.deg(self.body:getAngle()), 0, 60, 300, "left")
	-- love.graphics.printf("angle P "..self.body:getX().." "..self.body:getY(), 0, 40, 300, "left")
	
	-- local a = self.entity:getAngleTo(love.graphics.getWidth() / 3, love.graphics.getHeight() / 3)
	-- love.graphics.printf("angle P "..a, 0, 40, 300, "left")
	-- love.graphics.line(love.graphics.getWidth() / 3, 0, love.graphics.getWidth() / 3, love.graphics.getHeight())
	-- love.graphics.line(0, love.graphics.getHeight() / 3, love.graphics.getWidth(), love.graphics.getHeight() / 3)
end
