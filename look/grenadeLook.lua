

local grenadeLook_mt = {}
grenadeLook_mt.__index = grenadeLook_mt

grenadeHeight = 30
grenadeWidth = 15

function newGrenadeLook(world)
	local gl = {}
	
	gl.world = world
	gl.body = love.physics.newBody(world, 0, 0, "static")
	gl.body:setInertia(0)
	gl.body:setMassData(0, 0, 1, 0)
	gl.shape = love.physics.newRectangleShape(grenadeWidth, grenadeHeight)
	gl.mainFixture = love.physics.newFixture(gl.body, gl.shape, 1)
	gl.color = createColor(255, 255, 255)
	gl.speed = laserSpeed;

	return setmetatable(gl, grenadeLook_mt)
end

function grenadeLook_mt:setEntity(entity)
	self.entity = entity
	self.mainFixture:setUserData(entity);
end

function grenadeLook_mt:setPosition(x, y)
	self.body:setPosition(x, y)
end

function grenadeLook_mt:getPosition()
	return self.body:getPosition(x, y)
end

function grenadeLook_mt:getLinearVelocity()
	return self.body:getLinearVelocity()
end


function grenadeLook_mt:setAngularVelocity(v)
	self.body:setAngularVelocity(v)
end

function grenadeLook_mt:setAngle(a)
	a = math.fmod(a, 360)
	self.body:setAngle(math.rad(a))
	self.body:setLinearVelocity(math.sin(self.body:getAngle()) * self.speed, 
								-math.cos(self.body:getAngle()) * self.speed)	
end

function grenadeLook_mt:getAngle()
	return math.deg(self.body:getAngle())
end

function grenadeLook_mt:setSpeed(speed)
	self.speed = speed
	self.body:setLinearVelocity(math.sin(self.body:getAngle()) * speed, 
								-math.cos(self.body:getAngle()) * speed)
end

function grenadeLook_mt:update(dt)
	self.body:setAngularVelocity(0)
end

function grenadeLook_mt:explode()
end

function grenadeLook_mt:setColor(color)
end

function grenadeLook_mt:destroyBodies()
	if self.mainFixture ~= nil then
		self.mainFixture:destroy()
	end
	self.mainFixture = nil
end

function grenadeLook_mt:draw()
	love.graphics.setColor(self.color.red, self.color.green, self.color.blue)
	
	local x, y = self.body:getPosition()
	love.graphics.setColor(255, 255, 255)
	love.graphics.circle("fill", x, y, grenadeWidth, 100)
	
	local crossWidth = grenadeWidth / 3
	-- croix exterieur
	love.graphics.setColor(106, 84, 0)
	love.graphics.rectangle("fill", x - crossWidth / 2, y - (grenadeHeight + grenadeWidth / 2), 
									crossWidth, grenadeHeight - grenadeWidth / 2)
	love.graphics.rectangle("fill", x - grenadeWidth, y - (grenadeHeight), 
									2 * grenadeWidth, crossWidth)
	-- croix interieur
	love.graphics.setColor(201, 214, 134)
	love.graphics.rectangle("fill", x - grenadeWidth, y - (grenadeWidth / 6), 
									2 * grenadeWidth, grenadeWidth / 3)
	love.graphics.rectangle("fill", x - (grenadeWidth / 6), y - grenadeWidth, 
									grenadeWidth / 3, 2 * grenadeWidth)
    -- Affichage du lifetime
    if (self.entity.lifetime > 0) then
        love.graphics.print(math.ceil(self.entity.lifetime), x + grenadeWidth / 6, y - grenadeHeight - grenadeHeight / 2)
    end
									
	-- Affichage de la bounding box
	-- local topLeftX, topLeftY, bottomRightX, bottomRightY = self.fixture:getBoundingBox()
	-- love.graphics.rectangle("line", topLeftX, topLeftY, bottomRightX - topLeftX, bottomRightY - topLeftY)
end
