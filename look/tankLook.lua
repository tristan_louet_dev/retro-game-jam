local tankLook_mt = {}
tankLook_mt.__index = tankLook_mt

tankHeight = 150
tankWidth = 3 * tankHeight / 5
local turretHeight = 4 * tankHeight / 7
local turretWidth = 2 * tankWidth / 4
local canonHeight = 3 * tankHeight / 4
local canonWidth = 10
local clawsHeight = 3 * tankHeight / 4
local clawsWidth = 1 * tankWidth / 4
local clawsLinesStep = 5
local assHeight = 1 * tankHeight / 4
local assWidth = turretWidth

local assOffset = 9 * tankHeight/2/10

-- function filter(fix1, fix2)
	-- local ent1 = fix1:getUserData()
	-- local ent2 = fix2:getUserData()
	
	-- if (ent1 == nil) or (ent2 == nil) then
		-- return false
	-- end
	-- if (ent1.look == ent2.look) then
		-- return false
	-- end
	-- return true
-- end

function newTankLook(world)
	local gl = {}
	
	gl.world = world
    -- corps principal
	gl.tankBody = love.physics.newBody(world, 0, 0, "dynamic")
	gl.tankBody:setInertia(0)
	gl.tankBody:setMassData(0, 0, 100, 0)
	gl.tankShape = love.physics.newRectangleShape(tankWidth, tankHeight)
	gl.tankFixture = love.physics.newFixture(gl.tankBody, gl.tankShape, 1)
    
    -- tourelle
    --[[gl.turretBody = love.physics.newBody(world, 0, 0, "dynamic")
    gl.turretBody:setInertia(0)
    gl.turretBody:setMassData(0, 0, 1, 0)
    gl.turretShape = love.physics.newRectangleShape(turretWidth, turretHeight)
    gl.turretFixture = love.physics.newFixture(gl.turretBody, gl.turretShape, 1)
    gl.tank_turretJoint = love.physics.newRevoluteJoint(gl.tankBody, gl.turretBody, 0, 0, 0, 0, false)--]]
    -- canon
        -- rien
    -- ass
    
	gl.color = createColor(255, 255, 255)
	gl.speed = laserSpeed;
    gl.lightGrey = createColor(173, 173, 173)
    gl.mediumGrey = createColor(138, 138, 138)
    gl.darkGrey = createColor(99, 99, 99)

    gl.moveOffset = 0	
	-- world:setContactFilter(filter)
    
	return setmetatable(gl, tankLook_mt)
end

function tankLook_mt:setEntity(entity)
	self.initialized = true
	self.entity = entity
	self.tankFixture:setUserData(entity)
	--self.turretFixture:setUserData(entity)
end

function tankLook_mt:setPosition(x, y)
	self.tankBody:setPosition(x, y)
end

function tankLook_mt:getPosition()
    if self.entity:isDead() then
        return 0, 0
    end
	return self.tankBody:getPosition(x, y)
end

function tankLook_mt:setLinearVelocity(x, y)
	self.tankBody:setLinearVelocity(x, y)
end

function tankLook_mt:setAngularVelocity(v)
	self.tankBody:setAngularVelocity(v)
end

function tankLook_mt:setAngle(a)
	a = math.fmod(a, 360)
	self.tankBody:setAngle(math.rad(a))
	self.tankBody:setLinearVelocity(math.sin(self.tankBody:getAngle()) * self.speed, 
								-math.cos(self.tankBody:getAngle()) * self.speed)
end

function tankLook_mt:getAngle()
	return math.deg(self.tankBody:getAngle())
end

function tankLook_mt:setSpeed(speed)
	self.speed = speed
	self.tankBody:setLinearVelocity(math.sin(self.tankBody:getAngle()) * speed, 
								-math.cos(self.tankBody:getAngle()) * speed)
end

function tankLook_mt:update(dt)
	self.tankBody:setAngularVelocity(0)
end

function tankLook_mt:getLinearVelocity()
    return self.tankBody:getLinearVelocity()
end

function tankLook_mt:explode()
end

function tankLook_mt:setColor(color)
end

function tankLook_mt:destroyBodies()
	if self.tankFixture ~= nil then
		self.tankFixture:destroy()
	end
	self.tankFixture = nil
end

function tankLook_mt:draw()
	love.graphics.setColor(self.color.red, self.color.green, self.color.blue)
	
	-- corps principal
    
    love.graphics.setColor(getRGB(self.lightGrey))
    
    local x1, y1, x2, y2, x3, y3, x4, y4 = self.tankBody:getWorldPoints(-tankWidth / 2, -tankHeight / 2,
                                                                        tankWidth / 2, -tankHeight / 2,
                                                                        tankWidth / 2, tankHeight / 2,
                                                                        -tankWidth / 2, tankHeight / 2)
    
    love.graphics.polygon("fill", x1, y1, x2, y2, x3, y3, x4, y4)
    
    -- tourelle
    
    love.graphics.setColor(getRGB(self.mediumGrey))
    
    x1, y1, x2, y2, x3, y3, x4, y4 = self.tankBody:getWorldPoints(-turretWidth / 2, -turretHeight / 2,
                                                                  turretWidth / 2, -turretHeight / 2,
                                                                  turretWidth / 2, turretHeight / 2,
                                                                  -turretWidth / 2, turretHeight / 2)
    
    love.graphics.polygon("fill", x1, y1, x2, y2, x3, y3, x4, y4)
    
    -- canon
    
    love.graphics.setColor(getRGB(self.darkGrey))
    
    x1, y1, x2, y2, x3, y3, x4, y4 = self.tankBody:getWorldPoints(-canonWidth / 2, -canonHeight,
                                                                  canonWidth / 2, -canonHeight,
                                                                  canonWidth / 2, 0,
                                                                  -canonWidth / 2, 0)
    
    love.graphics.polygon("fill", x1, y1, x2, y2, x3, y3, x4, y4)
    
    -- claws
    
    --local xOffset = tankWidth/2 + clawsWidth
    --local yOffset = turretHeight/2 + 20
    local xOffset = tankWidth / 2 + clawsWidth / 2
    local yOffset = 0
    
    x1, y1, x2, y2, x3, y3, x4, y4 = self.tankBody:getWorldPoints(-clawsWidth / 2 -xOffset, -clawsHeight / 2 -yOffset,
                                                                  clawsWidth / 2 -xOffset, -clawsHeight / 2 -yOffset,
                                                                  clawsWidth / 2 -xOffset, clawsHeight / 2 -yOffset,
                                                                  -clawsWidth / 2 -xOffset, clawsHeight / 2 -yOffset)

    love.graphics.polygon("line", x1, y1, x2, y2, x3, y3, x4, y4)

    for i = -clawsHeight / 2 - yOffset, clawsHeight / 2 - yOffset, clawsLinesStep do
        x1, y1, x2, y2 = self.tankBody:getWorldPoints(-clawsWidth / 2 - xOffset, i, clawsWidth / 2 -xOffset, i)
        love.graphics.line(x1, y1, x2, y2)
    end
    
    x1, y1, x2, y2, x3, y3, x4, y4 = self.tankBody:getWorldPoints(-clawsWidth / 2 +xOffset, -clawsHeight / 2 +yOffset,
                                                                  clawsWidth / 2 +xOffset, -clawsHeight / 2 +yOffset,
                                                                  clawsWidth / 2 +xOffset, clawsHeight / 2 +yOffset,
                                                                  -clawsWidth / 2 +xOffset, clawsHeight / 2 +yOffset)

    love.graphics.polygon("line", x1, y1, x2, y2, x3, y3, x4, y4)
    
    for i = -clawsHeight / 2 - yOffset + 3, clawsHeight / 2 - yOffset, clawsLinesStep do
        x1, y1, x2, y2 = self.tankBody:getWorldPoints(-clawsWidth / 2 + xOffset, i, clawsWidth / 2 +xOffset, i)
        love.graphics.line(x1, y1, x2, y2)
    end
    
    --ass
    
    love.graphics.setColor(getRGB(convertLaserColor))

    x1, y1, x2, y2, x3, y3, x4, y4 = self.tankBody:getWorldPoints(-assWidth / 2, -assHeight / 2 +assOffset,
                                                                  assWidth / 2, -assHeight / 2 +assOffset,
                                                                  assWidth / 2, assHeight / 2 +assOffset,
                                                                  -assWidth / 2, assHeight / 2 +assOffset)

    local percent = self.entity:getAmmo(2) / self.entity:getMaxAmmo(2)
    local x41 = (x4 - x1) * percent
    local y41 = (y4 - y1) * percent
    local x32 = (x3 - x2) * percent
    local y32 = (y3 - y2) * percent
    love.graphics.polygon("fill", x4 - x41, y4 - y41, x3 - x32, y3 - y32, x3, y3, x4, y4)
    love.graphics.setColor(getRGB(self.darkGrey))
    love.graphics.polygon("line", x1, y1, x2, y2, x3, y3, x4, y4)
    
	-- Affichage de la bounding box
    -- love.graphics.setColor(255, 255, 255)
	-- local topLeftX, topLeftY, bottomRightX, bottomRightY = self.assFixture:getBoundingBox()
	-- love.graphics.rectangle("line", topLeftX, topLeftY, bottomRightX - topLeftX, bottomRightY - topLeftY)
    
    -- topLeftX, topLeftY, bottomRightX, bottomRightY = self.tankFixture:getBoundingBox()
    -- love.graphics.rectangle("line", topLeftX, topLeftY, bottomRightX - topLeftX, bottomRightY - topLeftY)
end