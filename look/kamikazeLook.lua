local kamikazeLook_mt = {}
kamikazeLook_mt.__index = kamikazeLook_mt

local kamikazeHeight = 30
local kamikazeWidth = 20

local kamikazeStartX = 0
local kamikazeStartY = 0

function newKamikazeLook(world)
	local e = {}
	
	e.world = world
	e.body = love.physics.newBody(world, kamikazeStartX, kamikazeStartX, "dynamic")
	e.body:setMassData(0, 0, 10, 0)
	e.shape = love.physics.newPolygonShape(kamikazeStartX - kamikazeWidth / 2, kamikazeStartX + kamikazeHeight / 2,
									kamikazeStartX + kamikazeWidth / 2, kamikazeStartX + kamikazeHeight / 2,
									kamikazeStartX, kamikazeStartX - kamikazeHeight / 2)
	e.fixture = love.physics.newFixture(e.body, e.shape, 1)
	e.color = createColor(138, 138, 138)
	e.speed = 0

	return setmetatable(e, kamikazeLook_mt)
end

function kamikazeLook_mt:setEntity(entity)
	self.entity = entity
	self.fixture:setUserData(entity);
end

function kamikazeLook_mt:setPosition(x, y)
	self.body:setPosition(x, y)
end

function kamikazeLook_mt:getPosition(x, y)
    return self.body:getPosition()
end

function kamikazeLook_mt:getLinearVelocity()
	return self.body:getLinearVelocity()
end

function kamikazeLook_mt:setAngularVelocity(v)
	self.body:setAngularVelocity(v)
end

function kamikazeLook_mt:setAngle(a)
	a = math.fmod(a, 360)
	self.body:setAngle(math.rad(a))
	self.body:setLinearVelocity(math.sin(self.body:getAngle()) * self.speed, 
								-math.cos(self.body:getAngle()) * self.speed)
end

function kamikazeLook_mt:getAngle()
	return math.deg(self.body:getAngle())
end

function kamikazeLook_mt:setSpeed(speed)
	self.speed = speed
	self.body:setLinearVelocity(math.sin(self.body:getAngle()) * speed, 
								-math.cos(self.body:getAngle()) * speed)
end

function kamikazeLook_mt:update(dt)
	self.body:setAngularVelocity(0)
end

function kamikazeLook_mt:explode()
end

function kamikazeLook_mt:setColor(color)
	self.color = color
end

function kamikazeLook_mt:destroyBodies()
	if self.fixture ~= nil then
		self.fixture:destroy()
	end
	self.fixture = nil
end

function kamikazeLook_mt:draw()
	love.graphics.setColor(self.color.red, self.color.green, self.color.blue)
	local x1, y1, x2, y2, x3, y3 = self.body:getWorldPoints(
									-kamikazeWidth / 2, kamikazeHeight / 2,
									kamikazeWidth / 2, kamikazeHeight / 2,
									0, -kamikazeHeight / 2)
	love.graphics.triangle("fill", x1, y1, x2, y2, x3, y3)
	-- Affichage de la bounding box
	-- local topLeftX, topLeftY, bottomRightX, bottomRightY = self.fixture:getBoundingBox()
	-- love.graphics.rectangle("line", topLeftX, topLeftY, bottomRightX - topLeftX, bottomRightY - topLeftY)
	
	-- love.graphics.printf("Vie K "..self.entity:getLife(), 0, 0, 300, "left")
	-- love.graphics.printf("angle K "..math.deg(self.body:getAngle()), 0, 0, 300, "left")
	-- love.graphics.printf("angle K "..self.body:getX().." "..self.body:getY(), 0, 20, 300, "left")
end
