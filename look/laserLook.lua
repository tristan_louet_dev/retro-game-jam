

local laserLook_mt = {}
laserLook_mt.__index = laserLook_mt

laserHeight = 10
laserWidth = 1
laserSpeed = 350

function newLaserLook(world, color)
	local ll = {}
	
	ll.world = world
	ll.body = love.physics.newBody(world, 0, 0, "dynamic")
	ll.body:setMassData(0, 0, 10, 0)
	ll.shape = love.physics.newRectangleShape(laserWidth, laserHeight)
	ll.fixture = love.physics.newFixture(ll.body, ll.shape, 1)
	ll.color = color
	ll.speed = laserSpeed;

	return setmetatable(ll, laserLook_mt)
end

function laserLook_mt:setEntity(entity)
	self.entity = entity
	self.fixture:setUserData(entity);
end

function laserLook_mt:setPosition(x, y)
	self.body:setPosition(x, y)
end

function laserLook_mt:getPosition()
	return self.body:getPosition(x, y)
end

function laserLook_mt:getLinearVelocity()
	return self.body:getLinearVelocity()
end

function laserLook_mt:setAngularVelocity(v)
	self.body:setAngularVelocity(v)
end

function laserLook_mt:setLinearVelocity(v)
	self.body:setLinearVelocity(v)
end

function laserLook_mt:setAngle(a)
	a = math.fmod(a, 360)
	self.body:setAngle(math.rad(a))
	self.body:setLinearVelocity(math.sin(self.body:getAngle()) * self.speed, 
								-math.cos(self.body:getAngle()) * self.speed)	
end

function laserLook_mt:getAngle()
	return math.deg(self.body:getAngle())
end

function laserLook_mt:setSpeed(speed)
	self.speed = speed
	self.body:setLinearVelocity(math.sin(self.body:getAngle()) * speed, 
								-math.cos(self.body:getAngle()) * speed)
end

function laserLook_mt:update(dt)
	self.body:setAngularVelocity(0)
end

function laserLook_mt:explode()
end

function laserLook_mt:setColor(color)
end

function laserLook_mt:destroyBodies()
	if self.fixture ~= nil then
		self.fixture:destroy()
	end
	self.fixture = nil
end

function laserLook_mt:draw()
	love.graphics.setColor(self.color.red, self.color.green, self.color.blue)
	x1, y1, x2, y2 = self.body:getWorldPoints(0, 0, 0, laserHeight)
	love.graphics.line(x1, y1, x2, y2)
	
	-- Affichage de la bounding box
	-- local topLeftX, topLeftY, bottomRightX, bottomRightY = self.fixture:getBoundingBox()
	-- love.graphics.rectangle("line", topLeftX, topLeftY, bottomRightX - topLeftX, bottomRightY - topLeftY)
end