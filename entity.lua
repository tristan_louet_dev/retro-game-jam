local entity_mt = {}
entity_mt.__index = entity_mt

local blinkDuration = 0.75
local blinkStep = 0.05
local chancesToLoot = 40
local damageLaserReloadPerSecond = 2
local maxExplosions = 5
local explosionsStep = 0.1

function newEntity(look, behavior, team)
	local ent = {}
	
	ent.look = look
	ent.life = 1
	ent.look:setEntity(ent)
	ent.behavior = behavior
	ent.team = team
    ent.lifetime = -1 -- -1 is immortal
    ent.isBlinking = false
    ent.blinkCountdown = 0
    ent.dtAccumulator = 0
    ent.blinkNow = false
    ent.chancesToLoot = chancesToLoot
    ent.damageLaserCount = 10
    ent.convertLaserCount = 1
    ent.shieldLaserCount = 10
    ent.maxDamageLaser = 50
    ent.maxShieldLaser = 10
    ent.maxConvertLaser = 2
	ent.timeToShot = 0
    ent.isExploding = false
    ent.explodingX = 0
    ent.explodingY = 0
    ent.explosions = {}

	return setmetatable(ent, entity_mt)
end

function entity_mt:update(dt)
	if not self:isDead() then
		self.timeToShot = math.max(self.timeToShot - dt, 0)
		if self.behavior ~= nil then
			self.behavior:update(dt, self)
		end
		self.look:update(dt)
        if self:getLifetime() > 0 then
            self:setLifetime(self:getLifetime() - 1*dt)
        end
        -- useful for the blink
        if self.isBlinking then
            self.dtAccumulator = self.dtAccumulator + dt
            self.blinkCountdown = self.blinkCountdown - dt
            if self.dtAccumulator >= blinkStep then
                self.dtAccumulator = 0
                self.blinkNow = not self.blinkNow
            end
            if self.blinkCountdown <= 0 then
                self.isBlinking = false
            end
        end
        
        -- reloads damage laser
        if self.timeToShot <= 0 then
            self:refillLaser(0, damageLaserReloadPerSecond * dt)
        end
	end
    -- updates explosions
    if self.isExploding then
        for i = 1, #self.explosions do
            local ex = self.explosions[i]
            if ex.remaningTime > 0 then
                ex.remaningTime = ex.remaningTime - dt
                ex.nextExplosionStep = ex.nextExplosionStep - dt
                if ex.nextExplosionStep <= 0 then
                    ex.nextExplosionStep = explosionsStep
                    if ex.isBig then
                        ex.radius = ex.radius - ex.radius / 2
                    else
                        ex.radius = ex.radius + ex.radius / 2
                    end
                    ex.isBig = not ex.isBig
                end
                
                if #self.explosions < maxExplosions then
                    -- 40 % de chance de faire un bébé explosion
                    if math.random(1, 40) <= 40 then
                        ex[#ex + 1] = newCircle(ex.x + math.random(-20, 20), ex.y + math.random(-20, 20), ex.radius - 10, 30)
                    end
                end
            else
                self.isExploding = false
            end
        end
    end
end

function newCircle(x, y, radius, time)
    local c = {}
    c.x = x
    c.y = y
    c.radius = radius
    c.nextExplosionStep = explosionsStep
    c.isBig = false
    c.remaningTime = time
    return c
end

function entity_mt:explode(x, y)
    if self.behavior ~= nil and self.behavior:explodes() then
        self.isExploding = true
        self.explosions = {}
        self.explosions[1] = newCircle(x, y, 30, 2)
        love.event.push("explodes")
    end
end

function entity_mt:getAmmo(value)
    if value == 0 then
       return self.damageLaserCount
    elseif value == 1 then
        return self.shieldLaserCount
    elseif value == 2 then
        return self.convertLaserCount
    else
        return 0
    end
end

function entity_mt:getMaxAmmo(value)
    if value == 0 then
       return self.maxDamageLaser
    elseif value == 1 then
        return self.maxShieldLaser
    elseif value == 2 then
        return self.maxConvertLaser
    else
        return 0
    end
end

function entity_mt:refillLaser(laserType, value)
    if laserType < 0 or laserType > 2 then
        self.life = self.life + value
    end
    
    local diff = self:getMaxAmmo(laserType) - self:getAmmo(laserType)
    if diff >= 0 and diff < value then
        value = diff
    end
    if laserType == 0 then
        self.damageLaserCount = self.damageLaserCount + value
    elseif laserType == 1 then
        self.shieldLaserCount = self.shieldLaserCount + value
    elseif laserType == 2 then
        self.convertLaserCount = self.convertLaserCount + value
    end
end

function entity_mt:getChancesToLoot()
    return self.chancesToLoot
end

function entity_mt:setChancesToLoot(value)
    self.chancesToLoot = value
end

function entity_mt:setLifetime(value)
    self.lifetime = value
end

function entity_mt:getLifetime()
    return self.lifetime
end

function entity_mt:draw()
    if self.isExploding then
        love.graphics.setColor(201, 214, 134)
        for i = 1, #self.explosions do
            local ex = self.explosions[i]
            love.graphics.circle("fill", ex.x, ex.y, ex.radius, 16)
        end
    end
    
	if not self:isDead() then
        -- gestion du blink
        if self.isBlinking and self.blinkNow then
            -- nothing
        else
            self.look:draw()
        end
	end
end

function entity_mt:setPosition(x, y)
	self.look:setPosition(x, y)
end

function entity_mt:getPosition()
	return self.look:getPosition()
end

function entity_mt:setAngle(a)
	self.look:setAngle(a)
end

function entity_mt:getAngle()
	return self.look:getAngle()
end

function entity_mt:getAngleTo(x0, y0)
    local x1, y1 = self:getPosition()
    local x = (x0 - x1)
    local y = -(y0 - y1)
    return math.fmod(360 - (math.deg(math.atan2(y, x)) + 90), 360)
end

function entity_mt:getLinearVelocity()
    return self.look:getLinearVelocity()
end

function entity_mt:setAngleCloser(ent)
	local x1, y1 = self:getPosition()
    local x2, y2 = ent:getPosition()
	local dx1 = x2 - x1
	local dy1 = y2 - y1
	local dx2, dy2 = 0, 0
	
	dx2, dy2 = self:getLinearVelocity()
	
	local d = dx2 * dy1 - dx1 * dy2
	if d > 0 then
		self:setAngle(self:getAngle() + 3)
	elseif d < 0 then
		self:setAngle(self:getAngle() - 3)
	end
end

function entity_mt:setAngularVelocity(v)
	self.look:setAngularVelocity(v)
end

function entity_mt:setSpeed(speed)
	self.look:setSpeed(speed)
end

function entity_mt:getLife()
	return self.life
end

function entity_mt:isImmunedToLaser(value)
    if self.behavior == nil then
        return false
    end
    return self.behavior:isImmunedToLaser(value)
end

function entity_mt:lastWhisper()
    local x, y = self:getPosition()
    if math.random(1, 100) <= self:getChancesToLoot() then
        love.event.push("died", x, y)
    end
    self:explode(x, y)
    self.look:destroyBodies()
end

function entity_mt:isDead()
	return self.life <= 0
end

function entity_mt:die()
	if not self:isDead() then
        self.life = 0
        self:lastWhisper()
	end
end

function entity_mt:contact(ent2)
	if ent2 ~= nil then
		if self.behavior ~= nil then
			self.behavior:contact(self, ent2)
		else
			if self:getTeam() ~= ent2:getTeam() then
				ent2:damage()
			end
		end
	end
end

function entity_mt:damage()
    self:blink()
	self.life = self.life - 1
    if self.life == 0 then
        self:lastWhisper()
    end
end

function entity_mt:setTeam(team)
	self.team = team
end

function entity_mt:canChangeTeam()
    if self.behavior == nil then
        return false
    end
    return self.behavior:canChangeTeam()
end

function entity_mt:getTeam()
	return self.team
end

function entity_mt:isOutWorld()
    local x, y = self:getPosition()
    local outX = x < 0 or x > love.graphics.getWidth()
    local outY = y < 25 or y > love.graphics.getHeight()
    return outX, outY
end

function entity_mt:isGone()
	local outX, outY = self:isOutWorld()
	return outX or outY
end

function entity_mt:getWorld()
	return self.look.world
end

function entity_mt:getDistance(e2)
    x1, y1 = self:getPosition()
    x2, y2 = e2:getPosition()
    return math.sqrt(math.pow(x2 - x1, 2) + math.pow(y2 - y1, 2))
end

function entity_mt:setColor(color)
	self.look:setColor(color)
end

function entity_mt:blink()
    self.isBlinking = true
    self.blinkCountdown = blinkDuration
end

function entity_mt:checkInWorld()
    local x, y = self:getPosition()
    if x < 0 then
        self:setAngle(90)
		self:setPosition(0, y)
    elseif x > love.graphics.getWidth() then
        self:setAngle(270)
		self:setPosition(love.graphics.getWidth(), y)
    elseif y < 25 then
        self:setAngle(180)
		self:setPosition(x, 25)
    elseif y > love.graphics.getHeight() then
        self:setAngle(0)
		self:setPosition(x, love.graphics.getHeight())
    end
end

-- laserType 0->degats, 1->bouclier, 2->convertir
function entity_mt:shoot(laserType, fireSpeed)
	if self.timeToShot <= 0 then
		local entity = nil
		if laserType == 0 then
            if self.damageLaserCount >= 1 then
                entity = newEntity(newLaserLook(self:getWorld(), damageLaserColor), newDamageLaserBehavior(self), self.team)
                self.damageLaserCount = self.damageLaserCount - 1
            end
		end
		if laserType == 1 then
            if self.shieldLaserCount >= 1 then
                entity = newEntity(newLaserLook(self:getWorld(), shieldLaserColor), newShieldLaserBehavior(self), self.team)
                self.shieldLaserCount = self.shieldLaserCount - 1
            end
		end
		if laserType == 2 then
            if self.convertLaserCount >= 1 then
                entity = newEntity(newLaserLook(self:getWorld(), convertLaserColor), newConvertLaserBehavior(self), self.team)
                self.convertLaserCount = self.convertLaserCount - 1
            end
		end
        if entity ~= nil then
			love.event.push("shot")
            local x, y = self:getPosition()
            local dx = math.sin(math.rad(self:getAngle())) * playerHeight
            local dy = -math.cos(math.rad(self:getAngle())) * playerHeight
            entity:setPosition(x + dx, y + dy)
            entity:setAngle(self:getAngle())
            entity:setChancesToLoot(0)
            addEntity(entity)
            self.timeToShot = 1 / fireSpeed
        end
	end
end

function entity_mt:setBehavior(behavior)
	self.behavior = behavior
end
