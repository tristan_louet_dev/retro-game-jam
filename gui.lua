local gui_mt = {}
gui_mt.__index = gui_mt

function newGui(entity)
	local gui = {}
	
	gui.entity = entity

	return setmetatable(gui, gui_mt)
end

function gui_mt:update(dt)

end

function gui_mt:draw()
	love.graphics.setColor(106, 84, 0)
	love.graphics.rectangle("fill", 1, 1, love.graphics.getWidth() - 2, 25)
	love.graphics.setColor(155, 102, 58)
	love.graphics.rectangle("line", 1, 1, love.graphics.getWidth() - 2, 25)
	
	
	love.graphics.setColor(255, 255, 255)
	love.graphics.printf("SCORE "..score, 10, 5, 200, "left")
	
	-- On dessine les barres de munitions
	-- Barre 1
	love.graphics.setColor(255, 255, 255)
	love.graphics.printf("K : ", 190, 5, 50, "left")
	love.graphics.setColor(damageLaserColor.red, damageLaserColor.green, damageLaserColor.blue)
	local percent = self.entity:getAmmo(0) * 100 / self.entity:getMaxAmmo(0)
	love.graphics.rectangle("fill", 210, 2, percent, 20)	
	love.graphics.setColor(255, 255, 255)
	love.graphics.rectangle("line", 210, 2, 100, 20)
	-- Barre 2
	love.graphics.setColor(255, 255, 255)
	love.graphics.printf("L : ", 330, 5, 50, "left")
	love.graphics.setColor(shieldLaserColor.red, shieldLaserColor.green, shieldLaserColor.blue)
	percent = self.entity:getAmmo(1) * 100 / self.entity:getMaxAmmo(1)
	love.graphics.rectangle("fill", 350, 2, percent, 20)
	love.graphics.setColor(255, 255, 255)
	love.graphics.rectangle("line", 350, 2, 100, 20)

	-- Barre 3
	love.graphics.setColor(255, 255, 255)
	love.graphics.printf("M : ", 470, 5, 50, "left")
	love.graphics.setColor(convertLaserColor.red, convertLaserColor.green, convertLaserColor.blue)
	percent = self.entity:getAmmo(2) * 100 / self.entity:getMaxAmmo(2)
	love.graphics.rectangle("fill", 490, 2, percent, 20)
	love.graphics.setColor(255, 255, 255)
	love.graphics.rectangle("line", 490, 2, 100, 20)

	-- On dessine la vie du joueur
	love.graphics.setColor(255, 255, 255)
	love.graphics.printf("Vie : ", 600, 5, 50, "left")
	local left = 630
	for i = 1, self.entity:getLife() do
		love.graphics.rectangle("fill", left, 2, 20, 20)
		left = left + 25
	end
end
