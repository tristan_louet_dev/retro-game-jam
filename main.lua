love.filesystem.load("entity.lua")()
love.filesystem.load("look/playerLook.lua")()
love.filesystem.load("look/kamikazeLook.lua")()
love.filesystem.load("behavior/kamikazeBehavior.lua")()
love.filesystem.load("look/laserLook.lua")()
love.filesystem.load("behavior/damageLaserBehavior.lua")()
love.filesystem.load("behavior/convertLaserBehavior.lua")()
love.filesystem.load("look/grenadeLook.lua")()
love.filesystem.load("behavior/grenadeBehavior.lua")()
love.filesystem.load("look/lootLook.lua")()
love.filesystem.load("behavior/lootBehavior.lua")()
love.filesystem.load("behavior/shooterBehavior.lua")()
love.filesystem.load("behavior/shieldBehavior.lua")()
love.filesystem.load("behavior/shieldLaserBehavior.lua")()
love.filesystem.load("gui.lua")()
love.filesystem.load("gameManager.lua")()
love.filesystem.load("look/tankLook.lua")()
love.filesystem.load("behavior/tankBehavior.lua")()
--love.filesystem.load("behavior/playerMouseBehavior.lua")()
love.filesystem.load("behavior/playerKeyboardBehavior.lua")()
love.filesystem.load("behavior/emptyBehavior.lua")()

function love.load(arg)
    love.graphics.setDefaultImageFilter("nearest", "nearest")
    menuImage = love.graphics.newImage("menu.png")
    love.graphics.setCaption("Hatstéroïdes")
    love.graphics.setMode(menuImage:getWidth(), menuImage:getHeight(), false, false, 0)
end

doTuto = false
menu = true

function love.update(dt)
    if menu then
        if love.mouse.isDown("l") then
            local x, y = love.mouse.getPosition()
            
            if (y >= 140 and y < 200) then
                menu = false
                doTuto = false
            elseif (y >= 200) then
                doTuto = true
                menu = false
                print("DO TUTO")
            end
            if not menu then
                loadGame(arg)
            end            
        end
    else
        updateGame(dt)
    end
end

function love.draw()
    if menu then
        love.graphics.draw(menuImage)
    else
        drawGame()
    end
end

-- events handlers ------
function love.handlers.died(x, y)
    createRandomLoot(x, y)
end

function love.handlers.assTouched(x, y)
    addEntity(createLoot(x, y, 2, 2))
end

function beginContact(fixture1, fixture2, c)
	local ent1 = fixture1:getUserData()
	local ent2 = fixture2:getUserData()
	
	c:setEnabled(false)
	c:setRestitution(0)
	if (ent1 ~= nil) and (ent2 ~= nil) then
		ent1:contact(ent2)
		ent2:contact(ent1)
	end
end

-- function post(fixture1, fixture2, c)
	-- local ent1 = fixture1:getUserData()
	-- local ent2 = fixture2:getUserData()
	
	-- ent1:setAngularVelocity(0)
	-- ent1:setAngularVelocity(0)
-- end

function loadGame(arg)
    -- love.graphics.setDefaultImageFilter("nearest", "nearest")
    love.graphics.setMode(800, 600, false, true, 0)
	
	sounds = {}
	sounds.explosion = love.audio.newSource("explosion.ogg")
	sounds.hit = love.audio.newSource("hit.ogg")
	sounds.shot = love.audio.newSource("shot.ogg")
    
    world = love.physics.newWorld(0, 0, true)
	world:setCallbacks(beginContact, nil, nil, nil)
    playerTeamName = "player"
	enemyTeamName = "enemy"
	gui = nil
	
	bossLife = nil
	
	gameManager = newGameManager()
	
    -- ## INITIALIZATION ## --
    -- OS detection
    local subChar = package.config:sub(1,1)
    operatingSystem = "unknown"
    if subChar == '/' then
        operatingSystem = "unix"
    elseif subChar == '\\' then
        operatingSystem = "windows"
    end
    keyMap = {}
    keyMap["a"] = "a"
    keyMap["q"] = "q"
    keyMap["z"] = "z"
    keyMap["s"] = "s"
    keyMap["d"] = "d"
    keyMap["l"] = "l"
    keyMap["k"] = "k"
    keyMap["m"] = "m"
    if operatingSystem == "windows" then
       keyMap["a"] = "q"
       keyMap["q"] = "a"
       keyMap["m"] = ";"
       keyMap["z"] = "w"
    end
    -- random initialization
    math.randomseed(os.time())
    
    -- utilities -------------
    damageLaserColor = createColor(196, 123, 117)
    shieldLaserColor = createColor(138, 123, 206)
    convertLaserColor = createColor(163, 230, 153)
	
	gui = newGui(gameManager:getAgents()[1])
end

function updateGame(dt)
    if (love.keyboard.isDown("escape")) then
      love.event.quit()
    end
    
    -- moteur
	--if not gameManager:isOver() then
		local agents = gameManager:getAgents()
		for i = 1, #agents do
			agents[i]:update(dt)
		end
		world:update(dt)
		gui:update(dt)
	--end
    ---------
	gameManager:update(dt)
end

function drawGame()
    local agents = gameManager:getAgents()
    for i = 1, #agents do
        agents[i]:draw()
    end
    gameManager:draw()
	gui:draw()
	if bossLife ~= nil then
		bossLife:draw()
	end
	if gameManager:isOver() then
		if gameManager:hasWon() then
			love.graphics.printf("Victoire !", 
								love.graphics.getWidth() / 2, love.graphics.getHeight() / 2, 300, "left")
		else
			love.graphics.printf("Game Over !", 
								love.graphics.getWidth() / 2, love.graphics.getHeight() / 2, 300, "left")
		end
	end
end

function addEntity(entity)
	gameManager:addEntity(entity)
end

function createColor(r, g, b)
	local color = {}
	color.red = r
	color.green = g
	color.blue = b
	return color
end

function getRGB(color)
    return color.red, color.green, color.blue
end

function createLoot(x, y, laserType, value)
    local color = createColor(255, 255, 255)
    if laserType == 0 then
        color = damageLaserColor
    elseif laserType == 1 then
        color = shieldLaserColor   
    elseif laserType == 2 then
        color = convertLaserColor
    end
    local look = newLootLook(world, x, y, color)
    local behavior = newLootBehavior(laserType, value)
    local loot = newEntity(look, behavior, "enemy")
    loot:setChancesToLoot(0)
    return loot
end

function createRandomLoot(x, y)
    local p = math.random(1, 100)
    if p <= 25 then
        addEntity(createLoot(x, y, 0, 6))
    elseif p <= 50 then
        addEntity(createLoot(x, y, 1, 4))
    elseif p <= 65 then
        addEntity(createLoot(x, y, 3, 1))
    else
        addEntity(createLoot(x, y, 2, 1))
    end
end

score = 0

function love.handlers.kill(points)
	score = score + points	
end

function love.handlers.explodes()
    sounds.explosion:play()
end

function love.handlers.hit(points)
	score = score + points
	sounds.hit:play()
end

function love.handlers.shot()
	sounds.shot:play()
end
