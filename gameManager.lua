local gameManager_mt = {}
gameManager_mt.__index = gameManager_mt

function newGameManager()
	local gm = {}
	
	gm.agents = {}
	-- On cree le joueur
	gm.agents[#gm.agents + 1] = newEntity(newPlayerLook(world), newPlayerBehavior(), playerTeamName)
	gm.agents[#gm.agents]:setPosition(love.graphics.getWidth() / 2, love.graphics.getHeight() - 20)
	gm.agents[#gm.agents].life = 5
	
	gm.task = tutoPhase1
    gm.toDraw = drawPhase1
    gm.inTuto = true
    
    if not doTuto then
        gm.task = initPhase1
        gm.toDraw = nil
    end
	
	gm.over = false
	gm.win = false
	
	gm.nextPhase = nil
	
	return setmetatable(gm, gameManager_mt)
end

function gameManager_mt:update(dt)
    if self.inTuto then
        if love.keyboard.isDown("backspace") then
            self.agents = {self.agents[1]}
            self.task = initPhase1
            self.inTuto = false
        elseif love.keyboard.isDown(keyMap["a"]) then
            self.agents = {self.agents[1]}
            self.task = initPhase5
            self.inTuto = false
        elseif love.keyboard.isDown("e") then
            self.agents = {self.agents[1]}
            self.task = initTankPhase
            self.inTuto = false
        end
    end
	self.task(self, dt)
end

function gameManager_mt:draw()
    if self.toDraw ~=nil then
        self.toDraw()
    end
end

function gameManager_mt:isOver()
	return self.over
end

function gameManager_mt:hasWon()
	return self.win
end

function gameManager_mt:getAgents()
	return self.agents
end

function gameManager_mt:isPlayer(entity)
	return entity == self.agents[1]
end

function gameManager_mt:getPlayer()
    return self.agents[1]
end

function gameManager_mt:addEntity(entity)
	self.agents[#self.agents + 1] = entity
end

function updatePhase(gm, dt)
	if gm.inTuto and love.keyboard.isDown(" ") then
        gm.agents={gm.agents[1]}
        if gm.nextPhase == nil then
            gm.over = true
            gm.win = true
        else
            gm.inTuto = false
            gm.toDraw = nil
            gm.task = gm.nextPhase
        end
    end
    if not inTuto and gm.agents[1]:isDead() then
		gm.over = true
		return
	end
    for i = 2, #gm.agents do
		if (not gm.agents[i]:isDead()) and (gm.agents[i]:getTeam() ~= gm.agents[1]:getTeam()) then
			return
		end
    end
	if gm.nextPhase == nil then
		gm.over = true
		gm.win = true
	else
		gm.task = gm.nextPhase
	end
end

function initPhase1(gm, dt)
	for i = 1, 3 do
        gm.agents[#gm.agents + 1] = newEntity(newKamikazeLook(world), newKamikazeBehavior(), enemyTeamName)
		gm.agents[#gm.agents]:setPosition(math.random(0, love.graphics.getWidth()), math.random(40, 300))
    end
	gm.nextPhase = initPhase2
	gm.task = updatePhase
	gm.agents[1].life = math.max(gm.agents[1].life + 1, 5)
end

function initPhase2(gm, dt)
	for i = 1, 3 do
        gm.agents[#gm.agents + 1] = newEntity(newKamikazeLook(world), newShooterBehavior(), enemyTeamName)
		gm.agents[#gm.agents]:setPosition(math.random(0, love.graphics.getWidth()), math.random(40, 200))
		gm.agents[#gm.agents]:setColor(createColor(152, 75, 67))
    end
	gm.nextPhase = initPhase3
	gm.task = updatePhase
	gm.agents[1].life = math.max(gm.agents[1].life + 1, 5)
end

function initPhase3(gm, dt)
	for i = 1, 1 do
        gm.agents[#gm.agents + 1] = newEntity(newKamikazeLook(world), newKamikazeBehavior(), enemyTeamName)
		gm.agents[#gm.agents]:setPosition(math.random(0, love.graphics.getWidth()), math.random(40, 300))
    end
	for i = 1, 1 do
        gm.agents[#gm.agents + 1] = newEntity(newKamikazeLook(world), newShooterBehavior(), enemyTeamName)
		gm.agents[#gm.agents]:setPosition(math.random(0, love.graphics.getWidth()), math.random(40, 200))
		gm.agents[#gm.agents]:setColor(createColor(152, 75, 67))
    end
	gm.nextPhase = initPhase4
	gm.task = updatePhase
	gm.agents[1].life = math.max(gm.agents[1].life + 1, 5)
end

function initPhase4(gm, dt)
	for i = 1, 3 do
        gm.agents[#gm.agents + 1] = newEntity(newKamikazeLook(world), newKamikazeBehavior(), enemyTeamName)
		gm.agents[#gm.agents]:setPosition(math.random(0, love.graphics.getWidth()), math.random(40, 300))
    end
	for i = 1, 2 do
        gm.agents[#gm.agents + 1] = newEntity(newKamikazeLook(world), newShooterBehavior(), enemyTeamName)
		gm.agents[#gm.agents]:setPosition(math.random(0, love.graphics.getWidth()), math.random(40, 200))
		gm.agents[#gm.agents]:setColor(createColor(152, 75, 67))
    end
	gm.nextPhase = initPhase5
	gm.task = updatePhase
	gm.agents[1].life = math.max(gm.agents[1].life + 1, 5)
end

function initPhase5(gm, dt)
	for i = 1, 1 do
        gm.agents[#gm.agents + 1] = newEntity(newKamikazeLook(world), newKamikazeBehavior(), enemyTeamName)
		gm.agents[#gm.agents]:setPosition(math.random(0, love.graphics.getWidth()), math.random(40, 300))
    end
	for i = 1, 1 do
        gm.agents[#gm.agents + 1] = newEntity(newKamikazeLook(world), newShooterBehavior(), enemyTeamName)
		gm.agents[#gm.agents]:setPosition(math.random(0, love.graphics.getWidth()), math.random(40, 200))
		gm.agents[#gm.agents]:setColor(createColor(152, 75, 67))
    end
	gm.agents[#gm.agents + 1] = newEntity(newGrenadeLook(world), newGrenadeBehavior(), enemyTeamName)
	gm.agents[#gm.agents]:setPosition(love.graphics.getWidth() / 2, love.graphics.getHeight() / 2)
	gm.agents[#gm.agents].life = 10
	gm.nextPhase = initTankPhase
	gm.task = updatePhase
	gm.agents[1].life = math.max(gm.agents[1].life + 1, 5)
end

function initTankPhase(gm, dt)
    gm.agents[#gm.agents + 1] = newEntity(newTankLook(world), newTankBehavior(), enemyTeamName)
	gm.agents[#gm.agents]:setPosition(love.graphics.getWidth() / 2, 0)
	gm.agents[#gm.agents]:setAngle(180)
    gm.agents[#gm.agents + 1] = newEntity(newKamikazeLook(world), newKamikazeBehavior(), enemyTeamName)
    gm.nextPhase = nil
    gm.task = updatePhase
	gm.agents[1].life = math.max(gm.agents[1].life + 1, 5)
end

-------------- TUTORIAL ------------------

function tutoPhase1(gm, dt)
    gm.toDraw = drawPhase1
    for i = 1, 10 do
        addEntity(newEntity(newKamikazeLook(world), newEmptyBehavior(), enemyTeamName))
        gm.agents[#gm.agents]:setPosition(math.random(0, love.graphics.getWidth()), math.random(40, 200))
    end
    gm.task = updatePhase
    gm.nextPhase = initPhase1
end

function drawPhase1()
    love.graphics.setColor(255, 255, 255)
    love.graphics.print("Pour passer le tutoriel : appuyez sur la touche backspace", 30, 30)
    love.graphics.print("Pour passer à la 5ème phase : appuyez sur 'a'. Pour le boss, appuyez sur 'e'.", 30, 50)
    love.graphics.print("Prenez un instant pour découvrir le vaisseau puis appuyez sur ESPACE lorsque vous serez prêt à combattre !", 30, love.graphics.getHeight() - 50)
    love.graphics.print("K: attaque de base | L: converti un ennemi en bouclier | M: converti un ennemi en attaquant", 30, love.graphics.getHeight() - 30)
end
