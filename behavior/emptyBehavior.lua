local emptyBehavior_mt = {}
emptyBehavior_mt.__index = emptyBehavior_mt

function newEmptyBehavior()
	local tb = {}
    
	return setmetatable(tb, emptyBehavior_mt)
end

function emptyBehavior_mt:canChangeTeam()
    return true
end

-- vitesse en pixels/seconde
local speed = 50

function emptyBehavior_mt:update(dt, ent)
end

function emptyBehavior_mt:contact(ent1, ent2)
    if ent1:getTeam() ~= ent2:getTeam() then
        ent1:setBehavior(newKamikazeBehavior())
        ent1:setSpeed(speed)
        ent2:damage()
    end
end

function emptyBehavior_mt:isImmunedToLaser(value)
    return false
end

function emptyBehavior_mt:explodes()
    return true
end

function emptyBehavior_mt:getHitPoint()
    return 0
end

function emptyBehavior_mt:getKillPoint()
    return 0
end
