local bossBar_mt = {}
bossBar_mt.__index = bossBar_mt

function newBossBar(boss)
	local bb = {}
    bb.boss = boss
	bb.originLife = boss.life
    
	return setmetatable(bb, bossBar_mt)
end

function bossBar_mt:draw()
	-- Affichage de la barre de vie
	love.graphics.setColor(255, 255, 255)
	local p = self.boss.life / self.originLife
	love.graphics.rectangle("fill", 0, 30, love.graphics.getWidth() * p, 20)
	love.graphics.rectangle("line", 0, 30, love.graphics.getWidth(), 20)
end



local tankBehavior_mt = {}
tankBehavior_mt.__index = tankBehavior_mt

local laserPoopTime = 10
local minionPoopTime = 8
local minMinionPoopTime = 3
local fireSpeed = 6

local tankPerception = 200

function newTankBehavior()
	local tb = {}
    tb.task = tank_init
	tb.timeToMinion = minionPoopTime
	tb.timeToPoop = 0
	tb.grenadePooped = false
    
	return setmetatable(tb, tankBehavior_mt)
end

function tankBehavior_mt:canChangeTeam()
    return false
end

-- vitesse en pixels/seconde
local speed = 50

function tankBehavior_mt:update(dt, ent)
    ent:refillLaser(2, 0.5 * dt)
	ent:checkInWorld()
    ent:setSpeed(speed)
	self.timeToPoop = math.max(0, self.timeToPoop - dt)
    self.timeToMinion = math.max(0, self.timeToMinion - dt)
	if self.task ~= nil then
		self.task(dt, self, ent)
	end
end

function tankBehavior_mt:contact(ent1, ent2)
	if (ent1:getTeam() ~= ent2:getTeam()) then
		ent2:damage()
	end
end

function tankBehavior_mt:isImmunedToLaser(value)
    return true
end

function tankBehavior_mt:explodes()
    return true
end

function tankBehavior_mt:getHitPoint()
    return 50
end

function tankBehavior_mt:getKillPoint()
    return 500
end

------------- MACHINE STATES -----------------

function tank_init(dt, self, ent)
    ent.life = 10
    ent.maxConvertLaser = 5
    ent:refillLaser(2, 5)
    ent:setSpeed(speed)
	
	self.task = tank_poping
end

function tank_poping(dt, self, ent)
	if ent:isGone() then
		ent:setAngle(180)
	else
		if self.timeToPoop <= 0 then
			self.task = tank_PoopAmmo
		elseif self.timeToMinion <= 0 then
			self.task = tank_PoopMinion
		else
			self.task = tank_Attack
		end
		if (bossLife == nil) then
			bossLife = newBossBar(ent)
		end
	end
end

function tank_PoopAmmo(dt, self, ent)
    --[[local dx, dy = ent:getLinearVelocity()
    dx = -dx
    dy = -dy
    local length = math.sqrt(math.pow(dx, 2) + math.pow(dy, 2))
    local nX = dx / length
    local nY = -dy / length
	local x, y = ent:getPosition()
    x = x + nX * (tankWidth)
    y = y - nY * (-tankHeight)--]]
    
    local x = math.random(10, love.graphics.getWidth() - 10)
    local y = math.random(10, love.graphics.getHeight() - 10)
    ent.convertLaserCount = ent.convertLaserCount - 2
	love.event.push("assTouched", x, y)
    
	self.timeToPoop = laserPoopTime
	self.task = tank_Attack
end

function tank_PoopMinion(dt, self, ent)
    if minionPoopTime > minMinionPoopTime then
        minionPoopTime = minionPoopTime - 1
    end
    addEntity(newEntity(newKamikazeLook(world), newKamikazeBehavior(), enemyTeamName))
    self.timeToMinion = minionPoopTime
    self.task = tank_Attack
end

function tank_Attack(dt, self, ent)
	if (not self.grenadePooped) and (ent:getLife() < bossLife.originLife / 2) then
		local grenade = newEntity(newGrenadeLook(world), newGrenadeBehavior(), enemyTeamName)
		grenade:setPosition(math.random(10, love.graphics.getWidth() - 10), math.random(40, love.graphics.getHeight() - 10))
		addEntity(grenade)
		self.grenadePooped = true
	end
    local player = gameManager:getPlayer()
    if ent:getDistance(player) <= tankPerception then
        ent:setAngleCloser(player)
        ent:shoot(0, fireSpeed)
    else
        ent:setSpeed(speed)
        ent:setAngle(ent:getAngle() + math.random(2) - math.random(2))
        if self.timeToPoop <= 0 then
            self.task = tank_PoopAmmo
        end
        if self.timeToMinion <= 0 then
            self.task = tank_PoopMinion
        end
    end
end
