

local shieldLaserBehavior_mt = {}
shieldLaserBehavior_mt.__index = shieldLaserBehavior_mt


function newShieldLaserBehavior(shooter)
	local dlb = {}
	
	dlb.shooter = shooter
	
	return setmetatable(dlb, shieldLaserBehavior_mt)
end

function shieldLaserBehavior_mt:getKillPoint()
	return 0
end

function shieldLaserBehavior_mt:getHitPoint()
	return 0
end

function shieldLaserBehavior_mt:update(dt, ent)
	if ent:isGone() then
		ent:die()
	end
end

function shieldLaserBehavior_mt:explodes()
    return false
end

function shieldLaserBehavior_mt:canChangeTeam()
    return false
end

function shieldLaserBehavior_mt:isImmunedToLaser(value)
    return false
end

function shieldLaserBehavior_mt:contact(ent1, ent2)
	if (ent1:getTeam() ~= ent2:getTeam() and ent2:canChangeTeam() and not ent2:isImmunedToLaser(1)) then
		ent2:setBehavior(newShieldBehavior(self.shooter))
		ent2:setColor(createColor(138, 123, 206))
		ent2:setTeam(ent1:getTeam())
		
		if (gameManager:isPlayer(self.shooter)) then
			love.event.push("kill", ent2.behavior:getHitPoint())
		end
	end
	if (ent2 ~= self.shooter) then
		ent1:die()
	end
end
