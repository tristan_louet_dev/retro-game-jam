local damageLaserBehavior_mt = {}
damageLaserBehavior_mt.__index = damageLaserBehavior_mt


function newDamageLaserBehavior(shooter)
	local dlb = {}
	
	dlb.shooter = shooter
	
	
	return setmetatable(dlb, damageLaserBehavior_mt)
end

function damageLaserBehavior_mt:getKillPoint()
	return 0
end

function damageLaserBehavior_mt:getHitPoint()
	return 0
end

function damageLaserBehavior_mt:update(dt, ent)
	if ent:isGone() then
		ent:die()
	end
end

function damageLaserBehavior_mt:explodes()
    return false
end

function damageLaserBehavior_mt:canChangeTeam()
    return false
end

function damageLaserBehavior_mt:isImmunedToLaser(value)
    return false
end

function damageLaserBehavior_mt:contact(ent1, ent2)
	if (ent1:getTeam() ~= ent2:getTeam() and not ent2:isImmunedToLaser(0)) then
		ent2:damage()
		if (ent2.behavior ~= nil and gameManager:isPlayer(self.shooter)) then
			if (ent2:isDead()) then
				love.event.push("kill", ent2.behavior:getKillPoint())
			else
				love.event.push("hit", ent2.behavior:getHitPoint())
			end
		end
	end
	if (ent2 ~= self.shooter) then
		ent1:die()
	end
end
