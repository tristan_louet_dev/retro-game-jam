

local convertLaserBehavior_mt = {}
convertLaserBehavior_mt.__index = convertLaserBehavior_mt


function newConvertLaserBehavior(shooter)
	local dlb = {}
	
	dlb.shooter = shooter
	
	return setmetatable(dlb, convertLaserBehavior_mt)
end


function convertLaserBehavior_mt:getKillPoint()
	return 0
end

function convertLaserBehavior_mt:getHitPoint()
	return 0
end

function convertLaserBehavior_mt:update(dt, ent)
	if ent:isGone() then
		ent:die()
	end
end

function convertLaserBehavior_mt:explodes()
    return false
end

function convertLaserBehavior_mt:canChangeTeam()
    return false
end

function convertLaserBehavior_mt:isImmunedToLaser(value)
    return true
end

function convertLaserBehavior_mt:contact(ent1, ent2)
	if (ent1:getTeam() ~= ent2:getTeam() and ent2:canChangeTeam() and not ent2:isImmunedToLaser(2)) then
		ent2:setTeam(ent1:getTeam())
		ent2:setColor(createColor(163, 230, 153))
		if (gameManager:isPlayer(self.shooter)) then
			love.event.push("kill", ent2.behavior:getHitPoint())
		end
	end
	if (ent2 ~= self.shooter) then
		ent1:die()
	end
end
