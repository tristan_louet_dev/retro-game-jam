local playerBehavior_mt = {}
playerBehavior_mt.__index = playerBehavior_mt

function newPlayerBehavior()
	local pb = {}
	
	pb.timeToShot = 0;

	return setmetatable(pb, playerBehavior_mt)
end

-- vitesse angulaire en degrés/seconde
local angularSpeed = 180
-- vitesse en pixels/seconde
local playerSpeed = 250
-- Le nombre de lasers / seconde
local fireSpeed = 10

function playerBehavior_mt:update(dt, ent)
	if love.keyboard.isDown(keyMap["z"]) then
		ent:setSpeed(playerSpeed)
	elseif love.keyboard.isDown(keyMap["s"]) then
		ent:setSpeed(-playerSpeed)
	else
		ent:setSpeed(0)
	end
	
	if love.keyboard.isDown(keyMap["q"]) then
		ent:setAngle(ent:getAngle() - angularSpeed * dt)
	elseif love.keyboard.isDown(keyMap["d"]) then
		ent:setAngle(ent:getAngle() + angularSpeed * dt)
	end
	
	if ((love.keyboard.isDown(keyMap["k"]) or love.keyboard.isDown(keyMap["l"]) or love.keyboard.isDown(keyMap["m"])) and (self.timeToShot <= 0)) then
		local entity = nil
		if love.keyboard.isDown(keyMap["k"]) then
			ent:shoot(0, fireSpeed)
		elseif love.keyboard.isDown(keyMap["l"]) then
			ent:shoot(1, fireSpeed)
		elseif love.keyboard.isDown(keyMap["m"]) then
			ent:shoot(2, fireSpeed)
		end
	end
	ent:checkInWorld()
end

function playerBehavior_mt:getLinearVelocity()
	return self.body:getLinearVelocity()
end

function playerBehavior_mt:isImmunedToLaser(value)
    return false
end

function playerBehavior_mt:contact(ent1, ent2)
	if (ent1:getTeam() ~= ent2:getTeam()) then
		ent2:damage()
	end
end

function playerBehavior_mt:explodes()
    return true
end

function playerBehavior_mt:canChangeTeam()
   return false 
end

