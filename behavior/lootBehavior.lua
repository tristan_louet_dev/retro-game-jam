local lootBehavior_mt = {}
lootBehavior_mt.__index = lootBehavior_mt

local lifetime = 10

function newLootBehavior(refillType, refillValue)
	local pb = {}
    pb.refillType = refillType
    pb.refillValue = refillValue
	
	pb.lifetimeSet = false

	return setmetatable(pb, lootBehavior_mt)
end

function lootBehavior_mt:getKillPoint()
	return 10
end

function lootBehavior_mt:getHitPoint()
	return 10
end

function lootBehavior_mt:update(dt, ent)
    if not self.lifetimeSet then
        ent:setLifetime(lifetime)
        self.lifetimeSet = true
    end
    if ent.lifetime <= 2.5 then
        ent:blink()
    end
    if ent.lifetime <= 0 then
        ent:die()
    end
end

function lootBehavior_mt:contact(ent1, ent2)
	if (ent1:getTeam() ~= ent2:getTeam()) then
		ent2:refillLaser(self.refillType, self.refillValue)
	end
end

function lootBehavior_mt:isImmunedToLaser(value)
    return true
end

function lootBehavior_mt:explodes()
    return false
end

function lootBehavior_mt:canChangeTeam()
   return false 
end

