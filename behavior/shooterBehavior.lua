local shooterBehavior_mt = {}
shooterBehavior_mt.__index = shooterBehavior_mt

local fireSpeed = 2

function newShooterBehavior()
	local kb = {}
    kb.task = shooter_searchEnemy
    kb.enemy = nil
    
	return setmetatable(kb, shooterBehavior_mt)
end

function shooterBehavior_mt:getKillPoint()
	return 150
end

function shooterBehavior_mt:getHitPoint()
	return 10
end

function shooterBehavior_mt:canChangeTeam()
    return true
end

function shooterBehavior_mt:isImmunedToLaser(value)
    return false
end

function shooterBehavior_mt:explodes()
    return true
end

-- vitesse angulaire en degres/seconde
local angularSpeed = 1
-- vitesse en pixels/seconde
local speed = 125
-- distance de "perception"
local perceptionDistance = 200

function shooterBehavior_mt:update(dt, ent)
    ent:setSpeed(speed)
	self.task(dt, self, ent)
end

function shooterBehavior_mt:contact(ent1, ent2)
	if (ent1:getTeam() ~= ent2:getTeam()) then
		ent2:damage()
	end
end

------------- MACHINE STATES -----------------

function shooter_searchEnemy(dt, self, ent)
    randomAngle(ent, 2)
    ent:checkInWorld()
    self.enemy = nil
    local enemyDistance = perceptionDistance
	local agents = gameManager:getAgents()
    for i = 1, #agents do
        if agents[i]:getTeam() ~= ent:getTeam() then
            if agents[i]:getDistance(ent) < enemyDistance then
                self.enemy = agents[i]
            end
        end
    end
    if self.enemy ~= nil then
        self.task = shooter_follow
    end
end

function shooter_follow(dt, self, ent)
    if (self.enemy:getTeam() == ent:getTeam()) or (self.enemy:isDead()) then
        self.enemy = nil
        self.task = shooter_searchEnemy
        return
    end
    ent:setAngleCloser(self.enemy)
	ent:shoot(0, fireSpeed)
    ent:checkInWorld()
end



------------- UTILS --------------------------

function randomAngle(ent, boundary)
    ent:setAngle(ent:getAngle() + math.random(-boundary) + math.random(boundary))
end
