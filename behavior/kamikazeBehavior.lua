local kamikazeBehavior_mt = {}
kamikazeBehavior_mt.__index = kamikazeBehavior_mt


function newKamikazeBehavior()
	local kb = {}
    kb.task = kamikaze_searchEnemy
    kb.enemy = nil
    
	return setmetatable(kb, kamikazeBehavior_mt)
end


function kamikazeBehavior_mt:getKillPoint()
	return 100
end

function kamikazeBehavior_mt:getHitPoint()
	return 10
end

function kamikazeBehavior_mt:canChangeTeam()
    return true
end

function kamikazeBehavior_mt:explodes()
    return true
end

function kamikazeBehavior_mt:isImmunedToLaser(value)
    return false
end

-- vitesse angulaire en degres/seconde
local angularSpeed = 1
-- vitesse en pixels/seconde
local speed = 125
-- distance de "perception"
local perceptionDistance = 250

function kamikazeBehavior_mt:update(dt, ent)
    ent:setSpeed(speed)
	self.task(dt, self, ent)
end

function kamikazeBehavior_mt:contact(ent1, ent2)
	if (ent1:getTeam() ~= ent2:getTeam()) then
		ent2:damage()
	end
end

------------- MACHINE STATES -----------------

function kamikaze_searchEnemy(dt, self, ent)
    randomAngle(ent, 2)
    ent:checkInWorld()
    self.enemy = nil
    local enemyDistance = perceptionDistance
	local friendDistance = 2 * perceptionDistance
	local closeFriend = nil
	local agents = gameManager:getAgents()
    for i = 1, #agents do
        if (agents[i]:getTeam() ~= ent:getTeam()) and (not agents[i]:isDead()) then
			local d = agents[i]:getDistance(ent)
            if d < enemyDistance then
                self.enemy = agents[i]
				enemyDistance = d
            end

        end
    end
    if self.enemy ~= nil then
        self.task = kamikaze_crash
    end
end

function kamikaze_crash(dt, self, ent)
    if (self.enemy:getTeam() == ent:getTeam()) or (self.enemy:isDead()) then
        self.enemy = nil
        self.task = kamikaze_searchEnemy
        return
    end
    ent:setAngleCloser(self.enemy)
    ent:checkInWorld()
end

------------- UTILS --------------------------

function randomAngle(ent, boundary)
    ent:setAngle(ent:getAngle() + math.random(-boundary) + math.random(boundary))
end
