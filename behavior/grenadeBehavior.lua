local lifetime = 10

local grenadeBehavior_mt = {}
grenadeBehavior_mt.__index = grenadeBehavior_mt

function newGrenadeBehavior()
	local gb = {}
    gb.task = searchEnemy
    gb.enemy = nil
    gb.lifetimeSet = false
    
	return setmetatable(gb, grenadeBehavior_mt)
end

function grenadeBehavior_mt:getKillPoint()
	return 500
end

function grenadeBehavior_mt:getHitPoint()
	return 30
end

function grenadeBehavior_mt:canChangeTeam()
    return false
end

function grenadeBehavior_mt:explodes()
    return true
end

function grenadeBehavior_mt:isImmunedToLaser(value)
    return false
end

function grenadeBehavior_mt:update(dt, ent)
    if not self.lifetimeSet then
        ent:setLifetime(lifetime)
        print(ent:getLifetime())
        self.lifetimeSet = true
    end
	local agents = gameManager:getAgents()
    if ent.lifetime <= 0 then
        for i =  1, #agents do
            agents[i]:die()
        end
    end
end

function grenadeBehavior_mt:contact(ent1, ent2)
	if (ent1:getTeam() ~= ent2:getTeam()) then
		ent2:damage()
	end
end
