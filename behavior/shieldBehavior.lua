local shieldBehavior_mt = {}
shieldBehavior_mt.__index = shieldBehavior_mt

local fireSpeed = 2

function newShieldBehavior(target)
	local kb = {}
    kb.task = shield_find
    kb.enemy = nil
	kb.target = target
	
	kb.angle = 0
    
	return setmetatable(kb, shieldBehavior_mt)
end

function shieldBehavior_mt:getKillPoint()
	return 20
end

function shieldBehavior_mt:getHitPoint()
	return 5
end

function shieldBehavior_mt:canChangeTeam()
    return false
end

function shieldBehavior_mt:isImmunedToLaser(value)
    return false
end

function shieldBehavior_mt:explodes()
    return true
end

-- vitesse angulaire en degres/seconde
local angularSpeed = 100
-- vitesse en pixels/seconde
local speed = 125
-- distance de "perception"
local perceptionDistance = 200

local shieldRadius = 50

function shieldBehavior_mt:update(dt, ent)
    ent:setSpeed(speed)
	self.task(dt, self, ent)
end

function shieldBehavior_mt:contact(ent1, ent2)
	if (ent1:getTeam() ~= ent2:getTeam()) then
		ent2:damage()
	end
end

------------- MACHINE STATES -----------------

function shield_find(dt, self, ent)
    if (self.target:isDead()) then
        ent:die()
        return
    end
    ent:setAngleCloser(self.target)
    ent:checkInWorld()
	local d = ent:getDistance(self.target)
	if (d <= shieldRadius) then
		local x, y = ent:getPosition()
		self.angle = self.target:getAngleTo(x, y) + 180
		self.task = shield_turn
		return
	end	
end

function shield_turn(dt, self, ent)
    if (self.target:isDead()) then
        ent:die()
        return
    end
	self.angle = self.angle + angularSpeed * dt
	local x, y = self.target:getPosition()
	x = x + math.sin(math.rad(self.angle)) * shieldRadius
	y = y - math.cos(math.rad(self.angle)) * shieldRadius
	ent:setPosition(x, y)
	ent:setAngle(self.angle + 90)
    ent:setSpeed(0)
    ent:checkInWorld()
	local d = ent:getDistance(self.target)
	if (d > (2 * shieldRadius)) then
		self.task = shield_find
	end
end

------------- UTILS --------------------------

function randomAngle(ent, boundary)
    ent:setAngle(ent:getAngle() + math.random(-boundary, boundary))
end
